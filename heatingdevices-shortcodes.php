<?php
defined( 'ABSPATH' ) or die( 'If you want to see what\'s in here, check out: https://gitlab.com/Mstrodl/heatingdevices-shortcodes' );
/*
Plugin Name: Heatingdevice's Shortcodes!
*/
$unit_info_path = 'https://gitlab.com/pocketrangers/pocketbot/raw/develop/assets/units.json'; // This can either be a URL to a JSON file or a path to a file (absolute or relative) on the drive
$unit_info_data = file_get_contents($unit_info_path);
$object_unit_info = json_decode($unit_info_data);
function objectToArray($d) {
    if (is_object($d))
        $d = get_object_vars($d);
    return is_array($d) ? array_map(__METHOD__, $d) : $d;
}
$unit_info = objectToArray($object_unit_info);
$d = array('n' => 'artillery'); // Default Unit
$t = array('n' => 'rapidartillery'); // Default Trait
$v = array('n' => 'rapidartillery'); // Default Weapon

// This doesn't actually get used xD
function get_unit_types() {
    foreach($unit_info['filters']['traits'] as &$name) {
        if(null !== $unit_info['filters']['traits'][$name]['wpn']) {
            array_push($traits_with_wpn, $name);
        }
    }
    foreach($unit_info['units'] as &$name) {
        if(null !== $unit_info['units'][$name]['label']) {
            array_push($units_with_label, $name);
        }
        if(null !== $unit_info['units'][$name]['desc']) {
            array_push($units_with_desc, $name);
        }
        if(null !== $unit_info['units'][$name]['tier']) {
            array_push($units_with_tier, $name);
        }
        if(null !== $unit_info['units'][$name]['cost']) {
            array_push($units_with_cost, $name);
        }
        if(null !== $unit_info['units'][$name]['longname']) {
            array_push($units_with_longname, $name);
        }
    }
    foreach($unit_info['weapons'] as &$name) {
        if(null !== $unit_info['weapons'][$name]['clip']) {
            array_push($weapons_with_clip, $name);
        }
        if(null !== $unit_info['weapons'][$name]['reload']) {
            array_push($weapons_with_reload, $name);
        }
    }
}
function get_name($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    if(null !== $unit_info['units'][$name]['name']) return $unit_info['units'][$name]['name']; else return '-';
}
function get_atk($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    if(null !== $unit_info['units'][$name]['atk']) return $unit_info['units'][$name]['atk']; else return '-';
}
function get_def($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    if(null !== $unit_info['units'][$name]['def']) return $unit_info['units'][$name]['def']; else return '-';
}
function get_traits($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    $traits = '';
    $traits_r = array();
    if(count($unit_info['units'][$name]['traits']) == 1) $traits = $unit_info['units'][$name]['traits'][0];
    if(count($unit_info['units'][$name][traits]) == 2) $traits = $unit_info['units'][$name]['traits'][0].' and '.$unit_info['units'][$name]['traits'][1];
    if(count($unit_info['units'][$name]['traits']) > 2) {
        $traits_r = $unit_info['units'][$name]['traits'];
        array_push($traits_r,'and '.array_pop($traits_r));
        $traits = implode(', ', $traits_r);
    }
    if(null !== $traits) return $traits; else return '-';
}
function get_label($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    if(null !== $unit_info['units'][$name]['label']) return $unit_info['units'][$name]['label']; else return '-';
}
function get_description($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    if(null !== $unit_info['units'][$name]['desc']) return $unit_info['units'][$name]['desc']; else return '-';
}
function get_tier($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    if(null !== $unit_info['units'][$name]['tier']) return $unit_info['units'][$name]['tier']; else return '-';
}
function get_price($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $d, $name )['n'];
    if($name == "mole") return '-';
    if(null !== $unit_info['units'][$name]['cost']) return $unit_info['units'][$name]['cost']; else return '-';
}
function get_trait_label($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $t, $name )['n'];
    if(null !== $unit_info['filters']['traits'][$name]['label']) return $unit_info['filters']['traits'][$name]['label']; else return '-';
}
function get_trait_desc($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $t, $name )['n'];
    if(null !== $unit_info['filters']['traits'][$name]['desc']) return $unit_info['filters']['traits'][$name]['desc']; else return '-';
}
function get_trait_wpn($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $t, $name )['n'];
    if(null !== $unit_info['filters']['traits'][$name]['wpn']) return $unit_info['filters']['traits'][$name]['wpn']; else return '-';
}
function get_wpn_cast($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $v, $name )['n'];
    if(null !== $unit_info['weapons'][$name]['cast']) return $unit_info['weapons'][$name]['cast']; else return '-';
}
function get_wpn_cool($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $v, $name )['n'];
    
    if(null !== $unit_info['weapons'][$name]['cool']) return $unit_info['weapons'][$name]['cool']; else return '-';
}
function get_wpn_atkrange($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $v, $name )['n'];
    if(null !== $unit_info['weapons'][$name]['AtkRange']) return $unit_info['weapons'][$name]['AtkRange']; else return '-';
}
function get_wpn_aggrange($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $v, $name )['n'];
    if(null !== $unit_info['weapons'][$name]['AggRange']) return $unit_info['weapons'][$name]['AggRange']; else return '-';
}
function get_wpn_clip($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $v, $name )['n'];
    if(null !== $unit_info['weapons'][$name]['clip']) return $unit_info['weapons'][$name]['clip']; else return '-';
}
function get_wpn_reload($name) {
    global $d, $t, $v, $unit_info;
    $name = shortcode_atts( $v, $name )['n'];
    if(null !== $unit_info['weapons'][$name]['reload']) return $unit_info['weapons'][$name]['reload']; else return '-';
}
function get_ucost($name) {
    global $d, $unit_info;
    $name = shortcode_atts( $v, $name )['n'];
    if($name == "mole") return '-';
    if(null !== $unit_info['units'][$name]['ucost']) return $unit_info['units'][$name]['ucost']; else return '-';
}
function register_shortcodes(){
    add_shortcode('unitsname', 'get_name');
    add_shortcode('unitsatk', 'get_atk');
    add_shortcode('unitsdef', 'get_def');
    add_shortcode('unitstraits', 'get_traits');
    add_shortcode('unitslabel', 'get_label');
    add_shortcode('unitsdesc', 'get_description');
    add_shortcode('unitstier', 'get_tier');
    add_shortcode('unitsprice', 'get_price');
    add_shortcode('traitslabel', 'get_trait_label');
    add_shortcode('traitsdesc', 'get_trait_desc');
    add_shortcode('traitswpn', 'get_trait_wpn');
    add_shortcode('wpncast', 'get_wpn_cast');
    add_shortcode('wpncool', 'get_wpn_cool');
    add_shortcode('wpnatkrange', 'get_wpn_atkrange');
    add_shortcode('wpnaggrange', 'get_wpn_aggrange');
    add_shortcode('wpnclip', 'get_wpn_clip');
    add_shortcode('wpnreload', 'get_wpn_reload');
    add_shortcode('unitscost', 'get_ucost');
}
add_action( 'init', 'register_shortcodes');
?>